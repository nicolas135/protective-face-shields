Here are several solutions for Protective Face Masks that we are developing and producing in Fablab ULB with the help of many collaborators.

See [this website](https://fablab-ulb.gitlab.io/projects/coronavirus/protective-face-shields/index-en/) for more information.

# Protocole de nettoyage des sur-masques


---

> ***[Téléchargez ici un protocole de désinfection réalisé avec le CHU Saint-Pierre de Bruxelles](Cleaning-protocol-fr.pdf)***

---



Les sur-masques sont une protection supplémentaire en **complément de votre masque validé**, et qui permet de protéger ces masques. Ils se composent d’un **support rigide et d’une visière, une feuille de PVC (Polyvinyl Chloride) souple.**

> ***Il faut nettoyer cette protection faciale dès le retrait.***

**ATTENTION :**

* Le sur-masque ne supporte pas une température supérieure à 50° C
* Le sur-masque n’est pas résistant à l’acétone
* Le sur-masque n’est pas résistant au chloroforme

![](images/hygiene-mask.jpg)

1. Retirer la visière de son support.
2. Poser la visière bien à plat
3. Nettoyer les deux faces, le support et les trous d’insertion sur le support :
   ✓ Spray désinfectant de surface (ex : Anios Surfa’safe ™, Incidin ™, Clinell ™, …)
4. Ne pas rincer pour assurer un contact suffisant entre le virucide (le produit désinfectant) et la visière
5. Replacer la visière sur le support

**Protocole à faire valider par les autorités compétentes de chaque institution. Le fablab ULB ne prend aucune responsabilité en cas de problème.**



## Compatibilité chimique des plastiques et autres ressources

* Plus d'informations sur la désinfection des masques imprimés en 3D sont disponibles sur le [site de Prusa](https://help.prusa3d.com/en/article/prusa-face-shield-disinfection_125457)
* Sprays de surface utilisés en milieu hospitalier pour désinfecter (virucide, bactéricide) :
   * [Clinell®](https://www.nu-careproducts.co.uk/brands/clinell.htm)
   * [Anios Surfa'safe®](https://sds.rentokil-initial.com/sds/files/GP-Rentokil-Surfasafe_FICHES_TECHNIQUES-FR-SDSxxxx_01_Technical_Sheet.pdf)
   * [Incidin®](https://www.verpa.be/content/files/artikelen/TECFR/3025780_TEC_FR.pdf)

* Le tableau ci-dessous indique la compatibilité des différents matériaux utilisés pour les masques avec des solvants typiques (**R : bonne résistance**, **L : résistance mais limitée**, **D : dégradation**).

|| [PVC](https://www.gilsoneng.com/reference/ChemRes.pdf) | [PLA](https://www.researchgate.net/figure/Map-of-chemical-compatibility-of-plastics-and-3-D-printing-materials-from-the-literature_fig2_326697946) | ABS ([1](http://www.kelco.com.au/wp-content/uploads/2009/02/abs-chemical-compatibility-guide.pdf), [2](https://www.plasticsintl.com/chemical-resistance-chart), [3](https://www.gilsoneng.com/reference/ChemRes.pdf))| [Plexi / PMMA](http://tools.thermofisher.com/content/sfs/brochures/D20480.pdf) | [Priplak/ Polypropylène (PP)](https://www.gilsoneng.com/reference/ChemRes.pdf)
|---|---|---|---|---|---|
|Ethanol |R| *L* |R| ***D*** |R|
|Isopropanol/2-propanol/alcool isopropylique |R|R| *L* | ***D*** |R
| 1-propanol|R|R| *L* ||R|
| Acétone | ***D*** | ***D*** | ***D*** | ***D*** |R|
